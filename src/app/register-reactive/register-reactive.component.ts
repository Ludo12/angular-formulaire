import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { User } from '../register/user';
import { debounceTime } from 'rxjs/operators';

function ratingRangeValidator(min: number, max: number): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value != null && (isNaN(c.value) || c.value < min || c.value > max)) {
      return { 'rangeError': true }
    }
    return null;
  };
}

// function ratingRangeValidator(c: AbstractControl): { [key: string]: boolean } | null {
//   if (c.value != null && (isNaN(c.value) || c.value < 1 || c.value > 5)) {
//     return { 'rangeError': true }
//   }
//   return null;
// }

function emailMatcher(c: AbstractControl): { [key: string]: boolean } | null {
  const emailControl = c.get('email');
  const emailConfirmControl = c.get('confirmEmail');

  //si pas remplie
  if (emailControl.pristine || emailConfirmControl.pristine) {
    return null;
  }

  //si egal
  if (emailControl.value === emailConfirmControl.value) {
    return null;
  }

  return { 'match': true };
}

@Component({
  selector: 'app-register-reactive',
  templateUrl: './register-reactive.component.html',
  styleUrls: ['./register-reactive.component.css']
})
export class RegisterReactiveComponent implements OnInit {

  public registerReactiveForm: FormGroup;

  public user: User = new User();

  public errorMsg: string;

  private validationErrorsMessages = {
    required: "Entrer votre E-mail",
    email: "L'E-mail n'est pas valide"
  };

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.registerReactiveForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.maxLength(20)]],
      lastname: ['', [Validators.required, Validators.minLength(4)]],
      emailGroup: this.fb.group({
        email: ['', [Validators.required, Validators.email]],
        confirmEmail: ['', Validators.required]
      }, { validators: emailMatcher }),
      phone: '',
      rating: [null, [ratingRangeValidator(1, 5)]],
      notification: 'email',
      sendCatalog: true,
      addresses: this.fb.array([this.createAddressGroup()])
    });
    // this.registerReactiveForm = new FormGroup({
    //   firstname: new FormControl(),
    //   lastname: new FormControl(),
    //   email: new FormControl(),
    //   sendCatalog: new FormControl(false)
    // });

    this.registerReactiveForm.get('notification').valueChanges.subscribe(value => this.setNotificationSetting(value));

    const emailControl = this.registerReactiveForm.get('emailGroup.email');
    emailControl.valueChanges.pipe(debounceTime(1000)).subscribe(val => { this.setMessage(emailControl) });
  }

  public get addressList(): FormArray {
    return this.registerReactiveForm.get('addresses') as FormArray;
  }

  public addAddress(): void {
    this.addressList.push(this.createAddressGroup())
  }

  public saveData() {
    console.log(this.registerReactiveForm);
    console.log('valeurs ', JSON.stringify(this.registerReactiveForm.value));
  }

  public fillFormData(): void {
    this.registerReactiveForm.patchValue({
      firstname: 'John',
      lastname: 'Doe B',
      emailGroup: { email: 'jdoe@test.com' },
      sendCatalog: false
    })
  }
  /**
   * setNotificationSetting
   */
  public setNotificationSetting(method: string): void {
    const phoneControl = this.registerReactiveForm.get('phone');
    if (method === 'text') {
      phoneControl.setValidators(Validators.required);
    } else {
      phoneControl.clearValidators();
    }
    phoneControl.updateValueAndValidity();
  }

  private createAddressGroup(): FormGroup {
    return this.fb.group({
      addressType: ['home'],
      street1: [''],
      street2: [''],
      city: [''],
      state: [''],
      zip: ['']
    })
  }
  private setMessage(val: AbstractControl): void {
    this.errorMsg = '';
    if ((val.touched || val.dirty) && val.errors) {
      this.errorMsg = Object.keys(val.errors).map(key => this.validationErrorsMessages[key]).join('')
    }
  }

}
